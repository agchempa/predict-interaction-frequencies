from noncmd_predictor import *
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
import numpy as np

def harness():
    reg = np.arange(0.0,1.0,0.1)
    train_errors = np.zeros(reg.shape)
    val_errors = np.zeros(reg.shape)
    test_errors = np.zeros(reg.shape)
    for i in range(len(reg)):
        val = reg[i]
        tr, va, tes = run_model('test_harness', epochs=200, lam=0, dropout=val)
        train_errors[i] = tr
        val_errors[i] = va
        test_errors[i] = tes
    plt.plot(reg, train_errors, label='Train Error')
    plt.plot(reg,val_errors, label='Validation Error')
    plt.plot(reg,test_errors, label='Test Error')
    plt.xlabel('Dropout rate')
    plt.ylabel('Loss')
    plt.legend()
    plt.title('Numerical search over dropout rate')
    plt.savefig('search_dropout_lim.png')

if __name__=='__main__':
    harness()
