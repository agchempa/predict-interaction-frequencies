print "Started imports"

import numpy as np
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, LSTM, Activation, Bidirectional
from sklearn.metrics import mean_squared_error
import argparse

print "Ended imports"

# Define globals
fraction_testing = 0.3
fraction_validation = 0.3
input_files = ["/scratch/PI/rondror/augustine/predict-contact-network/data/rep1.txt",
"/scratch/PI/rondror/augustine/predict-contact-network/data/rep2.txt",
"/scratch/PI/rondror/augustine/predict-contact-network/data/rep3.txt"]

def check_local_devices():
    '''Check if running on GPU'''
    from tensorflow.python.client import device_lib
    print device_lib.list_local_devices()

def check_data_dimensions():
    '''Diagnostic code to ensure input is correctly shaped'''
    print "Inputs: {}".format(model.input_shape)
    print "Outputs: {}".format(model.output_shape)
    print "Actual input: {}".format(data.shape)
    print "Actual output: {}".format(target.shape)

def extract_data(lookahead, window_size):
    data = []
    labels = []
    for filename in input_files:
        raw = np.loadtxt(filename)
        rawData = raw[:-lookahead]
        rawLabels = raw[1:]
        numFrames, numFeatures = rawData.shape
        numSequences = numFrames - window_size + 1
        fileData = np.empty((numSequences, window_size, numFeatures),dtype=float)
        fileLabels = np.empty((numSequences, window_size, numFeatures*lookahead),dtype=float)
        for i in range(numSequences):
            fileData[i, :, :] = rawData[i:i + window_size]
            for j in range(window_size):
                fileLabels[i, j, :] = rawLabels[i + j:i + j + lookahead].reshape((1, numFeatures*lookahead))
        data.append(fileData)
        labels.append(fileLabels)
    allData, allLabels = np.concatenate(data, axis=0), np.concatenate(labels, axis=0)
    num_sequences, _, inputsz = allData.shape

    # Keras now handles shuffling
    # shuffle data
    idx = np.random.permutation(allData.shape[0])
    allData, allLabels = allData[idx], allLabels[idx]

    # split data into training, validation, testing
    numTesting = int(num_sequences * fraction_testing)
    numValidation = int((num_sequences - numTesting) * fraction_validation)
    numTraining = int(num_sequences - numTesting - numValidation)

    # split data into training, validation, testing
    trainingData, trainingLabels = allData[:numTraining], allLabels[:numTraining]
    validationData, validationLabels = allData[numTraining:numTraining+numValidation], allLabels[numTraining:numTraining+numValidation]
    testingData, testingLabels = allData[numTraining+numValidation:], allLabels[numTraining+numValidation:]

    return trainingData, trainingLabels, validationData, validationLabels, testingData, testingLabels

def make_architecture(inputsz, outputsz, modelsz, params):
    # Unpack params
    dropout = params['dropout']
    lam = params['lambda']
    architecture = params['architecture']
    lossfunction = params['lossfunction']

    architecture = architecture.strip().split()
    model = Sequential()
    first = True
    for layer in architecture:
        if layer == 'BL':
            if first:
                model.add(Bidirectional(LSTM(outputsz, dropout=dropout, return_sequences=True), input_shape=modelsz))
                first = False
            else:
                model.add(Bidirectional(LSTM(outputsz, dropout=dropout, return_sequences=True)))
        if layer == 'L':
            if first:
                model.add(LSTM(outputsz, dropout=dropout, return_sequences=True, input_shape=modelsz))
                first = False
            else:
                model.add(LSTM(outputsz, dropout=dropout, return_sequences=True))
        if layer == 'R':
            if first:
                model.add(RNN(outputsz, dropout=dropout, return_sequences=True, input_shape=modelsz))
                first = False
            else:
                model.add(RNN(outputsz, dropout=dropout, return_sequences=True))
        if layer == 'D':
            if first:
                model.add(Dense(outputsz, activation='sigmoid', input_shape=modelsz))
                first = False
            else:
                model.add(Dense(outputsz, activation='sigmoid'))

    model.compile(loss=params['lossfunction'], optimizer='adam')
    return model

def run_model(runname, dropout=0.0, lookahead = 1, lossfunction='mse', windowsize = 20, epochs = 200, architecture = 'BL D', lam=0.2):
    params = {}
    params['dropout'] = dropout
    params['lookahead'] = lookahead
    params['lossfunction'] = lossfunction
    params['window_size'] = windowsize
    params['num_epochs'] = epochs
    params['architecture'] = architecture
    params['run_name'] = runname
    params['lambda'] = lam

    print "Reading in data"
    trainingData, trainingLabels, validationData, validationLabels, testingData, testingLabels = extract_data(params['lookahead'], params['window_size'])
    _, _, inputsz = testingData.shape
    _, _, outputsz = testingLabels.shape
    modelsz = (params['window_size'], inputsz)

    print "Setting up model"
    model = make_architecture(inputsz, outputsz, modelsz, params)
    '''
    model = Sequential()
    # model.add(Bidirectional(LSTM(outputsz, dropout=args.dropout, input_shape=(window_size, inputsz), return_sequences=True)))
    model.add(Bidirectional(LSTM(outputsz, dropout=dropout, return_sequences=True), input_shape=modelsz))
    model.add(Dense(outputsz, activation='sigmoid'))
    '''

    model.summary()

    print "Training model"
    model.fit(trainingData, trainingLabels, epochs=params['num_epochs'], verbose=2, validation_data=(validationData, validationLabels), shuffle=True)
    model.save('/scratch/PI/rondror/augustine/predict-contact-network/saved_models/%s.h5' % params['run_name'])

    training_loss = model.evaluate(trainingData, trainingLabels)
    validation_loss = model.evaluate(validationData, validationLabels)
    testing_loss = model.evaluate(testingData, testingLabels)

    print "\nTraining loss: %.6f" % training_loss
    print "\nValidation loss: %.6f" % validation_loss
    print "\nTesting loss: %.6f" % testing_loss

    return training_loss, validation_loss, testing_loss

    '''
    raw = np.loadtxt(input_files[0])
    window = raw[:20]
    insert = np.zeros((1,20, outputsz))
    insert[0,:,:] = window
    x = np.zeros((1,20))
    for l in range(20):
        out = model.predict(insert)[0,-1,:][None, :]
        window = np.concatenate((window[1:], out), axis=0)
        insert[0,:,:] = window
        x[0,l] = mean_squared_error(raw[20+l][None,:], out)
    plt.plot(x)
    np.save('Bidirectional.npy',x[0])
    plt.savefig('Bidirectional.png')
    '''

if __name__ == '__main__':
    run_model()
